package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/designUnit")
public class DesignUnitController {
  @Autowired
  IDesignUnitRepository gIDesignUnitRepository;
  @Autowired
  DesignUnitService gDesignUnitService;

  @GetMapping("/all")
  public ResponseEntity<List<DesignUnit>> getAllDesignUnit() {
    try {
      return new ResponseEntity<>(gDesignUnitService.getAllDesignUnit(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create")
  public ResponseEntity<Object> createDesignUnit(@Valid @RequestBody DesignUnit paramDesignUnit) {
    try {
      return new ResponseEntity<>(gDesignUnitService.createDesignUnit(paramDesignUnit), HttpStatus.CREATED);
    } catch (Exception e) {
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified DesignUnit: " + e.getCause().getCause().getMessage());
    }
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateDesignUnit(@PathVariable Integer id,
      @Valid @RequestBody DesignUnit paramDesignUnit) {
    Optional<DesignUnit> vDesignUnitData = gIDesignUnitRepository.findById(id);
    if (vDesignUnitData.isPresent()) {
      try {
        return new ResponseEntity<>(gDesignUnitService.updateDesignUnit(paramDesignUnit, vDesignUnitData),
            HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified DesignUnit: " + e.getCause().getCause().getMessage());
      }
    } else {
      DesignUnit vDesignUnitNull = new DesignUnit();
      return new ResponseEntity<>(vDesignUnitNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
    Optional<DesignUnit> vDesignUnitData = gIDesignUnitRepository.findById(id);
    if (vDesignUnitData.isPresent()) {
      try {
        gIDesignUnitRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      DesignUnit vDesignUnitNull = new DesignUnit();
      return new ResponseEntity<>(vDesignUnitNull, HttpStatus.NOT_FOUND);
    }
  }
}
