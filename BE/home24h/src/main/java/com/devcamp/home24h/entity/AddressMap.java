package com.devcamp.home24h.entity;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "address_map")
public class AddressMap {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "address", length = 5000, nullable = false)
  private String address;

  @Column(name = "_lat", nullable = false)
  private double latitude;

  @Column(name = "_lng", nullable = false)
  private double longitude;

  public AddressMap() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

}
