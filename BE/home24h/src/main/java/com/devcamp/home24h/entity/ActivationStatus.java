package com.devcamp.home24h.entity;

public enum ActivationStatus {
  YES("Y"),
  NO("N");

  private final String value;

  ActivationStatus(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
