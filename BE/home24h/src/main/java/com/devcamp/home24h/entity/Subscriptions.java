package com.devcamp.home24h.entity;

import javax.persistence.*;

@Entity
@Table(name = "subscriptions")
public class Subscriptions {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "user")
  private String user;

  @Column(name = "endpoint", columnDefinition = "LONGTEXT")
  private String endpoint;

  @Column(name = "publickey")
  private String publicKey;

  @Column(name = "authenticationtoken")
  private String authenticationToken;

  @Column(name = "contentencoding")
  private String contentEncoding;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getEndpoint() {
    return endpoint;
  }

  public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }

  public String getPublicKey() {
    return publicKey;
  }

  public void setPublicKey(String publicKey) {
    this.publicKey = publicKey;
  }

  public String getAuthenticationToken() {
    return authenticationToken;
  }

  public void setAuthenticationToken(String authenticationToken) {
    this.authenticationToken = authenticationToken;
  }

  public String getContentEncoding() {
    return contentEncoding;
  }

  public void setContentEncoding(String contentEncoding) {
    this.contentEncoding = contentEncoding;
  }

}
