package com.devcamp.home24h.entity;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "locations")
public class Locations {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "latitude", nullable = false)
  private float Latitude;

  @Column(name = "longitude", nullable = false)
  private float Longitude;

  public Locations() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public float getLatitude() {
    return Latitude;
  }

  public void setLatitude(float latitude) {
    Latitude = latitude;
  }

  public float getLongitude() {
    return Longitude;
  }

  public void setLongitude(float longitude) {
    Longitude = longitude;
  }

}
