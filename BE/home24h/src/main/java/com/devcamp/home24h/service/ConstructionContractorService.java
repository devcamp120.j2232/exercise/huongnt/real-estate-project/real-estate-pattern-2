package com.devcamp.home24h.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.home24h.entity.ConstructionContractor;
import com.devcamp.home24h.repository.IConstructionContractorRepository;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class ConstructionContractorService {
  @Autowired
  IConstructionContractorRepository gIConstructionContractorRepository;

  public ArrayList<ConstructionContractor> getAllConstructionContractor() {
    ArrayList<ConstructionContractor> listConstructionContractor = new ArrayList<>();
    gIConstructionContractorRepository.findAll().forEach(listConstructionContractor::add);
    return listConstructionContractor;
  }

  public ConstructionContractor createConstructionContractor(ConstructionContractor pConstructionContractor) {
    try {
      ConstructionContractor vConstructionContractorSave = gIConstructionContractorRepository
          .save(pConstructionContractor);
      return vConstructionContractorSave;
    } catch (Exception e) {
      return null;
    }
  }

  public ConstructionContractor updateConstructionContractor(ConstructionContractor pConstructionContractor,
      Optional<ConstructionContractor> pConstructionContractorData) {
    try {
      ConstructionContractor vConstructionContractor = pConstructionContractorData.get();
      vConstructionContractor.setName(pConstructionContractor.getName());
      vConstructionContractor.setAddress(pConstructionContractor.getAddress());
      vConstructionContractor.setDescription(pConstructionContractor.getDescription());
      vConstructionContractor.setEmail(pConstructionContractor.getEmail());
      vConstructionContractor.setFax(pConstructionContractor.getFax());
      vConstructionContractor.setNote(pConstructionContractor.getNote());
      vConstructionContractor.setPhone(pConstructionContractor.getPhone());
      vConstructionContractor.setPhone2(pConstructionContractor.getPhone2());
      vConstructionContractor.setProjects(pConstructionContractor.getProjects());
      vConstructionContractor.setWebsite(pConstructionContractor.getWebsite());
      ConstructionContractor vConstructionContractorSave = gIConstructionContractorRepository
          .save(vConstructionContractor);
      return vConstructionContractorSave;
    } catch (Exception e) {
      return null;
    }
  }
}
