package com.devcamp.home24h.entity;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "province")
public class Province {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "_name")
  private String name;

  @Column(name = "_code")
  private String code;

  @OneToMany(mappedBy = "province")
  private List<District> districts;

  @OneToMany(mappedBy = "province")
  private List<Project> projects;

  @OneToMany(mappedBy = "province")
  private List<RealeState> realeStates;

  @OneToMany(mappedBy = "province")
  private List<Street> streets;

  @OneToMany(mappedBy = "province")
  private List<Ward> wards;

  public Province() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public List<District> getDistricts() {
    return districts;
  }

  public void setDistricts(List<District> districts) {
    this.districts = districts;
  }

  public List<Project> getProjects() {
    return projects;
  }

  public void setProjects(List<Project> projects) {
    this.projects = projects;
  }

  public List<RealeState> getRealeStates() {
    return realeStates;
  }

  public void setRealeStates(List<RealeState> realeStates) {
    this.realeStates = realeStates;
  }

  public List<Street> getStreets() {
    return streets;
  }

  public void setStreets(List<Street> streets) {
    this.streets = streets;
  }

  public List<Ward> getWards() {
    return wards;
  }

  public void setWards(List<Ward> wards) {
    this.wards = wards;
  }

}
