package com.devcamp.home24h.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.home24h.entity.*;
import com.devcamp.home24h.repository.*;
import com.devcamp.home24h.service.*;

@RestController
@CrossOrigin
@RequestMapping("/addressMap")
public class AddressMapController {
  @Autowired
  IAddressMapRepository gIAddressMapRepository;
  @Autowired
  AddressMapService gAddressMapService;

  @GetMapping("/all")
  public ResponseEntity<List<AddressMap>> getAllAddressMap() {
    try {
      return new ResponseEntity<>(gAddressMapService.getAllAddressMap(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create")
  public ResponseEntity<Object> createAddressMap(@Valid @RequestBody AddressMap paramAddressMap) {
    try {
      return new ResponseEntity<>(gAddressMapService.createAddressMap(paramAddressMap), HttpStatus.CREATED);
    } catch (Exception e) {
      return ResponseEntity.unprocessableEntity()
          .body("Failed to Create specified AddressMap: " + e.getCause().getCause().getMessage());
    }
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateAddressMap(@PathVariable Integer id,
      @Valid @RequestBody AddressMap paramAddressMap) {
    Optional<AddressMap> vAddressMapData = gIAddressMapRepository.findById(id);
    if (vAddressMapData.isPresent()) {
      try {
        return new ResponseEntity<>(gAddressMapService.updateAddressMap(paramAddressMap, vAddressMapData),
            HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified AddressMap: " + e.getCause().getCause().getMessage());
      }
    } else {
      AddressMap vAddressMapNull = new AddressMap();
      return new ResponseEntity<>(vAddressMapNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteProductById(@PathVariable Integer id) {
    Optional<AddressMap> vAddressMapData = gIAddressMapRepository.findById(id);
    if (vAddressMapData.isPresent()) {
      try {
        gIAddressMapRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      AddressMap vAddressMapNull = new AddressMap();
      return new ResponseEntity<>(vAddressMapNull, HttpStatus.NOT_FOUND);
    }
  }
}
